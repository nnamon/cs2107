# CS2107 Assignment 2

by Jeremy Heng (A0146789H)

## 1. What is the ip-address and mac address of the host?

Assuming that the host refers to the machine on which the Wireshark
The IP address of the host is 172.25.104.112 and the MAC address is
90:00:4e:34:b7:db.

I obtained the IP address by looking at the statistics of the endpoints. The
first heuristic is that the 'host' IP address should appear the most number of
times in the pcap.

![Host IP](images/ipstats.png)

Indeed, it seems that there are HTTP requests originating from this IP address
as well.

## 2a. What is the manufacturer of the Network Card (NC) of the host?

Hon Hai Precision Industry Co., Ltd. aka Foxconn Technology Group

![Network Card Manufacturer](images/macadd.png)

## 2b. What is the range of the addresses correspond to the same manufacturer given in question (a)?

Since Organizationally Unique Identifiers are 24 bits (3 bytes), the range of
addresses corresponding to the manfacturer is 90:00:4E:00:00:00 to
90:00:4E:FF:FF:FF.

## 3. What is the ip-address and mac address of the DNS sever the host is using?

The IP address is 137.132.85.2. Although the ethernet frame in the reply says
that 00:0c:cf:42:19:80 is the MAC address of the source (i.e. the DNS server),
we cannot be entire sure this is the actual MAC address as we do not know if the
ethernet frame was overwritten as it passes through hops around the internet.

![DNS Server IP](images/dnsipmac.png)

## 4. What are the ports used by the host and the DNS server for the DNS query?

I assume the question is looking for both **source** and **destination** ports.
The number of DNS queries made (even excluding local solutions such as MDNS and
LLMNR) are too numerous to list manually. Hence, I wrote a script to automate
the process.

```python
from scapy.all import *

pkts = rdpcap("data_3.pcap")

for i in pkts:
    if DNS in i:
        sip = i[IP].src
        dip = i[IP].dst
        sport = i[UDP].sport
        dport = i[UDP].dport
        if sip == "172.25.104.112":
            summ = i.summary()[19:-1]
            print "(%s:%d) -> (%s:%d) [%s]" % (sip, sport,
                                               dip, dport, summ)
```

Note the Scapy dependency and the need to convert the provided `.pcapng` into
the classic libpcap `.pcap` file. Here is the output of the script:

```console
$ python extract_dns.py
(172.25.104.112:64654) -> (137.132.85.2:53) ["wpad.comp.nus.edu.sg."]
(172.25.104.112:54462) -> (137.132.85.2:53) ["ia.media-imdb.com."]
(172.25.104.112:52714) -> (137.132.85.2:53) ["encrypted-tbn2.gstatic.com."]
(172.25.104.112:61120) -> (137.132.85.2:53) ["encrypted-tbn2.gstatic.com."]
(172.25.104.112:65434) -> (137.132.85.2:53) ["encrypted-tbn2.gstatic.com."]
(172.25.104.112:49489) -> (137.132.85.2:53) ["e4642.g.akamaiedge.net."]
(172.25.104.112:61424) -> (137.132.85.2:53) ["e4642.g.akamaiedge.net."]
(172.25.104.112:65323) -> (137.132.85.2:53) ["encrypted-tbn1.gstatic.com."]
(172.25.104.112:52266) -> (137.132.85.2:53) ["encrypted-tbn1.gstatic.com."]
(172.25.104.112:53054) -> (137.132.85.2:53) ["encrypted-tbn0.gstatic.com."]
(172.25.104.112:60346) -> (137.132.85.2:53) ["encrypted-tbn3.gstatic.com."]
(172.25.104.112:62674) -> (137.132.85.2:53) ["encrypted-tbn1.gstatic.com."]
(172.25.104.112:53210) -> (137.132.85.2:53) ["encrypted-tbn3.gstatic.com."]
(172.25.104.112:61738) -> (137.132.85.2:53) ["encrypted-tbn0.gstatic.com."]
(172.25.104.112:51651) -> (137.132.85.2:53) ["encrypted-tbn3.gstatic.com."]
(172.25.104.112:57254) -> (137.132.85.2:53) ["encrypted-tbn0.gstatic.com."]
(172.25.104.112:57080) -> (137.132.85.2:53) ["www.imdb.com."]
(172.25.104.112:64654) -> (137.132.85.2:53) ["us.dd.imdb.com."]
(172.25.104.112:60030) -> (137.132.85.2:53) ["us.dd.imdb.com."]
(172.25.104.112:58535) -> (137.132.85.2:53) ["z-ecx.images-amazon.com."]
(172.25.104.112:61229) -> (137.132.85.2:53) ["d1ge0kk1l5kms0.cloudfront.net."]
(172.25.104.112:64654) -> (137.132.85.2:53) ["d1ge0kk1l5kms0.cloudfront.net."]
(172.25.104.112:61663) -> (137.132.85.2:53) ["pubads.g.doubleclick.net."]
(172.25.104.112:56486) -> (137.132.85.2:53) ["partnerad.l.doubleclick.net."]
(172.25.104.112:64654) -> (137.132.85.2:53) ["i.imdb.com."]
(172.25.104.112:61222) -> (137.132.85.2:53) ["partnerad.l.doubleclick.net."]
(172.25.104.112:64654) -> (137.132.85.2:53) ["d3k42v5me62qu0.cloudfront.net."]
(172.25.104.112:54628) -> (137.132.85.2:53) ["d3k42v5me62qu0.cloudfront.net."]
(172.25.104.112:55808) -> (137.132.85.2:53) ["shopbop.sp1.convertro.com."]
(172.25.104.112:49767) -> (137.132.85.2:53) ["aax-us-east.amazon-adsystem.com."]
(172.25.104.112:59509) -> (137.132.85.2:53) ["aax-us-east.amazon-adsystem.com."]
(172.25.104.112:53235) -> (137.132.85.2:53) ["aax-us-east.amazon-adsystem.com."]
(172.25.104.112:64654) -> (137.132.85.2:53) ["shopbop.sp1.convertro.com."]
(172.25.104.112:58194) -> (137.132.85.2:53) ["shopbop.sp1.convertro.com."]
(172.25.104.112:65112) -> (137.132.85.2:53) ["g-ecx.images-amazon.com."]
(172.25.104.112:64654) -> (137.132.85.2:53) ["d1ge0kk1l5kms0.cloudfront.net."]
(172.25.104.112:50727) -> (137.132.85.2:53) ["s.media-imdb.com."]
(172.25.104.112:54352) -> (137.132.85.2:53) ["fls-na.amazon.com."]
(172.25.104.112:59732) -> (137.132.85.2:53) ["us.dd.imdb.com."]
(172.25.104.112:59540) -> (137.132.85.2:53) ["fls-na.amazon.com."]
(172.25.104.112:53885) -> (137.132.85.2:53) ["fls-na.amazon.com."]
(172.25.104.112:57721) -> (137.132.85.2:53) ["www.amazon.com."]
(172.25.104.112:60663) -> (137.132.85.2:53) ["www.amazon.com."]
(172.25.104.112:59258) -> (137.132.85.2:53) ["www.amazon.com."]
(172.25.104.112:64654) -> (137.132.85.2:53) ["www.facebook.com."]
(172.25.104.112:65107) -> (137.132.85.2:53) ["i.media-imdb.com."]
(172.25.104.112:51162) -> (137.132.85.2:53) ["s.amazon-adsystem.com."]
(172.25.104.112:53822) -> (137.132.85.2:53) ["d3k42v5me62qu0.cloudfront.net."]
(172.25.104.112:55913) -> (137.132.85.2:53) ["star-mini.c10r.facebook.com."]
(172.25.104.112:58561) -> (137.132.85.2:53) ["star-mini.c10r.facebook.com."]
(172.25.104.112:65476) -> (137.132.85.2:53) ["platform.twitter.com."]
(172.25.104.112:49351) -> (137.132.85.2:53) ["cs472.wac.edgecastcdn.net."]
(172.25.104.112:64654) -> (137.132.85.2:53) ["cs472.wac.edgecastcdn.net."]
(172.25.104.112:61375) -> (137.132.85.2:53) ["twitter.com."]
(172.25.104.112:64654) -> (137.132.85.2:53) ["twitter.com."]
(172.25.104.112:65434) -> (137.132.85.2:53) ["s.amazon-adsystem.com."]
(172.25.104.112:62529) -> (137.132.85.2:53) ["twitter.com."]
(172.25.104.112:58110) -> (137.132.85.2:53) ["s.amazon-adsystem.com."]
(172.25.104.112:56842) -> (137.132.85.2:53) ["syndication.twitter.com."]
(172.25.104.112:64654) -> (137.132.85.2:53) ["cdn.syndication.twimg.com."]
(172.25.104.112:58451) -> (137.132.85.2:53) ["syndication.twitter.com."]
(172.25.104.112:64654) -> (137.132.85.2:53) ["syndication.twitter.com."]
(172.25.104.112:58849) -> (137.132.85.2:53) ["syndication.twimg.com."]
(172.25.104.112:57157) -> (137.132.85.2:53) ["syndication.twimg.com."]
(172.25.104.112:57062) -> (137.132.85.2:53) ["cm.g.doubleclick.net."]
(172.25.104.112:59269) -> (137.132.85.2:53) ["ads.yahoo.com."]
(172.25.104.112:64654) -> (137.132.85.2:53) ["tap.rubiconproject.com."]
(172.25.104.112:64655) -> (137.132.85.2:53) ["us-u.openx.net."]
(172.25.104.112:50799) -> (137.132.85.2:53) ["image5.pubmatic.com."]
(172.25.104.112:58743) -> (137.132.85.2:53) ["ib.adnxs.com."]
(172.25.104.112:56512) -> (137.132.85.2:53) ["www.burstnet.com."]
(172.25.104.112:57334) -> (137.132.85.2:53) ["bh.contextweb.com."]
(172.25.104.112:58111) -> (137.132.85.2:53) ["t4.liverail.com."]
(172.25.104.112:58431) -> (137.132.85.2:53) ["pix.btrll.com."]
(172.25.104.112:58149) -> (137.132.85.2:53) ["sync.adaptv.advertising.com."]
(172.25.104.112:58813) -> (137.132.85.2:53) ["sync.search.spotxchange.com."]
(172.25.104.112:50173) -> (137.132.85.2:53) ["pagead.l.doubleclick.net."]
(172.25.104.112:49978) -> (137.132.85.2:53) ["pagead.l.doubleclick.net."]
(172.25.104.112:53454) -> (137.132.85.2:53) ["fd-world.ngd.gysm.yahoodns.net."]
(172.25.104.112:62156) -> (137.132.85.2:53) ["fd-world.ngd.gysm.yahoodns.net."]
(172.25.104.112:49313) -> (137.132.85.2:53) ["us-u.openx.net."]
(172.25.104.112:59141) -> (137.132.85.2:53) ["us-u.openx.net."]
(172.25.104.112:49519) -> (137.132.85.2:53) ["tap-1798223565.us-east-1.elb.amazonaws.com."]
(172.25.104.112:54642) -> (137.132.85.2:53) ["tap-1798223565.us-east-1.elb.amazonaws.com."]
(172.25.104.112:61300) -> (137.132.85.2:53) ["e6603.g.akamaiedge.net."]
(172.25.104.112:58395) -> (137.132.85.2:53) ["e6603.g.akamaiedge.net."]
(172.25.104.112:64654) -> (137.132.85.2:53) ["ib.sin1.geoadnxs.com."]
(172.25.104.112:58176) -> (137.132.85.2:53) ["ib.sin1.geoadnxs.com."]
(172.25.104.112:56917) -> (137.132.85.2:53) ["fbstatic-a.akamaihd.net."]
(172.25.104.112:58306) -> (137.132.85.2:53) ["fbcdn-profile-a.akamaihd.net."]
(172.25.104.112:59907) -> (137.132.85.2:53) ["fbcdn-photos-d-a.akamaihd.net."]
(172.25.104.112:58176) -> (137.132.94.2:53) ["ib.sin1.geoadnxs.com."]
(172.25.104.112:64654) -> (137.132.85.2:53) ["liverail.c10r.facebook.com."]
(172.25.104.112:64655) -> (137.132.85.2:53) ["adsynth-pixel-548573282.us-east-1.elb.amazonaws.com."]
(172.25.104.112:54436) -> (137.132.85.2:53) ["liverail.c10r.facebook.com."]
(172.25.104.112:52786) -> (137.132.85.2:53) ["adsynth-pixel-548573282.us-east-1.elb.amazonaws.com."]
(172.25.104.112:49166) -> (137.132.85.2:53) ["burstnet-com-ob-644761938.us-west-2.elb.amazonaws.com."]
(172.25.104.112:60110) -> (137.132.85.2:53) ["burstnet-com-ob-644761938.us-west-2.elb.amazonaws.com."]
(172.25.104.112:58029) -> (137.132.85.2:53) ["gtm.sync.search.spotxchange.com.akadns.net."]
(172.25.104.112:54381) -> (137.132.85.2:53) ["gtm.sync.search.spotxchange.com.akadns.net."]
(172.25.104.112:58176) -> (137.132.87.2:53) ["ib.sin1.geoadnxs.com."]
(172.25.104.112:61964) -> (137.132.85.2:53) ["linkedfastly.contextweb.com."]
(172.25.104.112:60853) -> (137.132.85.2:53) ["linkedfastly.contextweb.com."]
(172.25.104.112:64654) -> (137.132.85.2:53) ["a1168.dsw4.akamai.net."]
(172.25.104.112:65293) -> (137.132.85.2:53) ["a1168.dsw4.akamai.net."]
(172.25.104.112:56945) -> (137.132.85.2:53) ["a2047.dspl.akamai.net."]
(172.25.104.112:65422) -> (137.132.85.2:53) ["a1842.dspmm1.akamai.net."]
(172.25.104.112:65455) -> (137.132.85.2:53) ["a2047.dspl.akamai.net."]
(172.25.104.112:64654) -> (137.132.85.2:53) ["a1842.dspmm1.akamai.net."]
(172.25.104.112:64704) -> (137.132.85.2:53) ["log-zone-b-1020666695.ap-southeast-1.elb.amazonaws.com."]
(172.25.104.112:59295) -> (137.132.85.2:53) ["log-zone-b-1020666695.ap-southeast-1.elb.amazonaws.com."]
(172.25.104.112:64654) -> (137.132.85.2:53) ["secure.imdb.com."]
(172.25.104.112:57177) -> (137.132.85.2:53) ["pro.imdb.com."]
(172.25.104.112:65008) -> (137.132.85.2:53) ["amazon.com."]
(172.25.104.112:59964) -> (137.132.85.2:53) ["amazon.com."]
(172.25.104.112:65243) -> (137.132.85.2:53) ["amazon.com."]
(172.25.104.112:61561) -> (137.132.85.2:53) ["vassg142.ocsp.omniroot.com."]
(172.25.104.112:52373) -> (137.132.85.2:53) ["a1158.b.akamai.net."]
(172.25.104.112:56826) -> (137.132.85.2:53) ["a1158.b.akamai.net."]
(172.25.104.112:64655) -> (137.132.85.2:53) ["pro.imdb.com."]
(172.25.104.112:65476) -> (137.132.85.2:53) ["pro.imdb.com."]
(172.25.104.112:53289) -> (137.132.85.2:53) ["secure.imdb.com."]
(172.25.104.112:51319) -> (137.132.85.2:53) ["secure.imdb.com."]
(172.25.104.112:55501) -> (137.132.85.2:53) ["pagead2.googlesyndication.com."]
(172.25.104.112:54478) -> (137.132.85.2:53) ["pagead46.l.doubleclick.net."]
(172.25.104.112:50065) -> (137.132.85.2:53) ["pagead46.l.doubleclick.net."]
(172.25.104.112:52894) -> (137.132.85.2:53) ["m.imdb.com."]
(172.25.104.112:50205) -> (137.132.85.2:53) ["www.gettyimages.com."]
(172.25.104.112:56614) -> (137.132.85.2:53) ["adclick.g.doubleclick.net."]
(172.25.104.112:55905) -> (137.132.85.2:53) ["pagead.l.doubleclick.net."]
(172.25.104.112:62362) -> (137.132.85.2:53) ["e11695.b.akamaiedge.net."]
(172.25.104.112:53977) -> (137.132.85.2:53) ["e11695.b.akamaiedge.net."]
(172.25.104.112:50240) -> (137.132.85.2:53) ["m.imdb.com."]
(172.25.104.112:49691) -> (137.132.85.2:53) ["m.imdb.com."]
(172.25.104.112:64947) -> (137.132.85.2:53) ["z-ecx.images-amazon.com."]
(172.25.104.112:49605) -> (137.132.85.2:53) ["g-ecx.images-amazon.com."]
(172.25.104.112:64654) -> (137.132.85.2:53) ["d1ge0kk1l5kms0.cloudfront.net."]
(172.25.104.112:54569) -> (137.132.85.2:53) ["d1ge0kk1l5kms0.cloudfront.net."]
(172.25.104.112:53151) -> (137.132.85.2:53) ["d1ge0kk1l5kms0.cloudfront.net."]
(172.25.104.112:57036) -> (137.132.85.2:53) ["d1ge0kk1l5kms0.cloudfront.net."]
(172.25.104.112:60622) -> (137.132.85.2:53) ["www.google.com."]
(172.25.104.112:51651) -> (137.132.85.2:53) ["www.google.com."]
(172.25.104.112:50372) -> (137.132.85.2:53) ["www.google.com."]
(172.25.104.112:60440) -> (137.132.85.2:53) ["wpad.comp.nus.edu.sg."]
(172.25.104.112:50870) -> (137.132.85.2:53) ["pmcdeadline2.files.wordpress.com."]
(172.25.104.112:65098) -> (137.132.85.2:53) ["s8.files.wordpress.com."]
(172.25.104.112:53112) -> (137.132.85.2:53) ["s8.files.wordpress.com."]
(172.25.104.112:56930) -> (137.132.85.2:53) ["pmcdeadline2.wordpress.com."]
(172.25.104.112:62093) -> (137.132.85.2:53) ["vip-lb.wordpress.com."]
(172.25.104.112:61029) -> (137.132.85.2:53) ["vip-lb.wordpress.com."]
(172.25.104.112:55266) -> (137.132.85.2:53) ["deadline.com."]
(172.25.104.112:56639) -> (137.132.85.2:53) ["deadline.com."]
(172.25.104.112:57989) -> (137.132.85.2:53) ["deadline.com."]
(172.25.104.112:62609) -> (137.132.85.2:53) ["0.gravatar.com."]
(172.25.104.112:56487) -> (137.132.85.2:53) ["cs91.wac.edgecastcdn.net."]
(172.25.104.112:51270) -> (137.132.85.2:53) ["cs91.wac.edgecastcdn.net."]
(172.25.104.112:50630) -> (137.132.85.2:53) ["wpad.comp.nus.edu.sg."]
(172.25.104.112:64654) -> (137.132.85.2:53) ["wpad.comp.nus.edu.sg."]
```

## 5. Which browser is the host using? What is its version?

The `User-Agent` header sent by the host is: `Mozilla/5.0 (Windows NT 6.1;
rv:44.0) Gecko/20100101 Firefox/44.0`.

Firefox user agents are of the format: `Mozilla/5.0 (platform; rv:geckoversion)
Gecko/geckotrail Firefox/firefoxversion`

Thus, we can see that the browser the host is using is Mozilla Firefox and the
version is 44.0.

## 6. What OS is the host using, Windows Vista, Windows 7 or Windows 10?

Windows NT 6.1 refers to Windows 7 or Windows Server 2008 R2. So, the answer is
probably Windows 7.

## 7a. The host asked for several images from 118.215.87.223 including an image with a lady. Show that image.

118.215.87.223 is the IP address to an Akamai Content Distribution Network
server. First, we filter the relevant packets and export them into a separate
.pcap file.

![Filtering the Packets](images/akamaifilter.png)

Then we export all the HTTP objects to a folder.

![Exporting HTTP objects](images/exportobjects.png)

I assume that you want a representative image of the 'Lady', Anne Hathaway,
among the money of her obtained and thus, will only include the profile image
IMDB uses as the biography image.

![Anne Hathaway](images/annehathaway.jpg)

## 7b. The host was visiting a website when most of the images were downloaded. Give that website.

The website is: http://www.imdb.com/name/nm0004266/.

![HTTP Statistics](images/imdbstats.png)

## 7c. Some images are repeatedly requested. Give a reason why the same image is being repeatedly requested.

The 'same' images are of different sizes. The browser requests for the 'same'
image because one image of a small size might act as a thumbnail that expands to
a full sized one on click.

## 8. The host also checked his/her email during the session. Does the host receive new email? Does the host send out new email?

The email traffic, if it exists, is encrypted behind TLS. Let's take a look at
the IP addresses that might be involved and their reverse DNS entries:

1. 137.132.90.87 - stuimaphost0.comp.nus.edu.sg

This looks like an IMAP server. This means that there was an attempt to retrieve
email messages from the server however we cannot be sure that there were
messages to retrieve since the communication is hidden behind SSL.

There does not seem to be any NUS SMTP servers in use anywhere in the pcap.

## 9a. What is the rate of 172.25.104.112 for Ipv4?

0.0439 ms

![IP Statistics](images/ipstats.png)

## 9b. What is the average speed of the capture (B/s)?

0.432 MBit/s or 432000 Bits/s.

![Capture Summary](images/capturespeed.png)

## 10. Give other information about the system or the network.

### SSDP

There are Windows 8 hosts (172.25.101.34, 172.25.96.108, and 172.25.103.223)
broadcasting the presence of plug and play devices.

```
NOTIFY * HTTP/1.1
Host: 239.255.255.250:1900
Cache-Control: max-age=4
Location: 172.25.101.34:53294
NT: uuid:4E50646A-B607-4ECB-9676-8DC10ABE8A5F
NTS: ssdp:alive
SERVER: windows/6.2 IntelUSBoverIP:1/1
USN: uuid:4E50646A-B607-4ECB-9676-8DC10ABE8A5F::IntelUSBoverIP:1
```

### IPv6

There were 26 IPv6 conversations. Most of them were name resolutions over the
local network such as LLMNR and MDNS.

```
00000000  00 00 00 00 00 01 00 00  00 00 00 00 0a 4a 6f 68 ........ .....Joh
00000010  6e 27 73 20 4d 61 63 08  5f 61 69 72 70 6c 61 79 n's Mac. _airplay
00000020  04 5f 74 63 70 05 6c 6f  63 61 6c 00 00 10 80 01 ._tcp.lo cal.....
```

Most of the machines that were generating such traffic are Apple machines.

### Received Email

This is speculation but since the first HTTP request happens right after the TLS
connection to the imap server is one that attempts to fetch the Anne Hathaway
biography image without any prior HTML page being fetched, perhaps the email is
a HTML encoded message with the image of Anne Hathaway included. This is further
substantiated with the lack of a referrer header.

![No Referer Header](images/noreferrer.png)

### Local Network Address Mapping

Here are some IPv4 and IPv6 mapping to the local network devices discovered.

```
172.25.101.22   Lis-MacBook-Pro-4.local
172.25.101.30   Rickards-MacBook-Pro.local
172.25.106.70   Tuan-Phans-iPhone.local
172.25.97.99    iPhone-9.local
172.25.98.171   Vins-iPhone-2.local
172.25.96.246   iPhone-7.local
172.25.97.81    Pontuss-MacBook-Pro.local
172.25.97.174   jiaxude-iPhone.local
e80::7256:81ff:fe97:50ad       Pontuss-MacBook-Pro.local
fe80::aebc:32ff:fe8b:a6c3       Rickards-MacBook-Pro.local
fe80::147f:fc67:a42c:6207       iPhone-9.local
fe80::1610:9fff:fed0:2edb       Lis-MacBook-Pro-4.local
fe80::c9b:ad6d:b248:e18 Vins-iPhone-2.local
fe80::10fc:e045:a5b:1372        Tuan-Phans-iPhone.local
fe80::43c:df4c:66fb:70ac        jiaxude-iPhone.local
fe80::9b:23f2:47e4:d348 iPhone-7.local
```

