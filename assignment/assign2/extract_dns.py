from scapy.all import *

pkts = rdpcap("data_3.pcap")

for i in pkts:
    if DNS in i:
        sip = i[IP].src
        dip = i[IP].dst
        sport = i[UDP].sport
        dport = i[UDP].dport
        if sip == "172.25.104.112":
            summ = i.summary()[19:-1]
            print "(%s:%d) -> (%s:%d) [%s]" % (sip, sport,
                                               dip, dport, summ)

