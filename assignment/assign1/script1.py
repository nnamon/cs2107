from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

you = "jeremycs2107@gmail.com"
me = you

msg = MIMEMultipart('alternative')

fhash = "9113da1b61a4999f144088c6fd4e91df"
textdata = "assignment1 10\njeremy_heng_wen_ming_A0146789H.pdf\n%s" % fhash
htmldata = ('<div dir="ltr"><div>assignment1 10</div><div>' +
            'jeremy_heng_wen_ming_A0146789H.pdf</div>%s<br></div>' % fhash)

part1 = MIMEText(textdata, 'text')
part2 = MIMEText(htmldata, 'html')

msg.attach(part1)
msg.attach(part2)
msg['Subject'] = 'jeremy heng wen ming A0146789H'
namefrom = "Professor Jones <%s>" % you
msg['From'] = namefrom
msg['To'] = namefrom

s = smtplib.SMTP('localhost')
s.sendmail(me, [you], msg.as_string())
s.quit()
